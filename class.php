<?php
class Htmlpage{
    protected $title="Please Insert Title"; #default title
    protected $body="Please Insert Body"; #default body
    function __construct($title="",$body=""){
        if($title!=""){
        $this->title=$title;
        }
        if($body!=""){
        $this->body=$body;
        }
    }
    public function view(){ 
        echo "<html>  
                <head>
                <title>$this->title</title>
                </head>
                <body>
                $this->body
                </body>
            </html>";
        }
    }

class coloredMessage extends Htmlpage{
    protected $color='red'; #default color
    public function __set($property,$value){
        if($property=='color') {
            $colors = array('green','purple','blue','pink'); #Proper colors
            if(in_array($value,$colors)){
                $this->color=$value;               
            } 
            else {
                $this->body='Color Is Not Valid, Please Chose OTHER Color (green,purple,blue,pink)'; #error message 
            }
        } 
    }    
    
             
    public function view(){
        echo "<html>  
                <head>
                <title>$this->title</title>
                </head>
                <body>
                <P style ='color:$this->color'>$this->body</p>
                </body>
            </html>";
       
    }
}    

class fontSize extends coloredMessage{    
        protected $size=20;
        public function __set($property,$value){
            parent:: __set($property,$value);
                if ($property=='size'){
                    $sizes = range(10,24); #Proper sizes
                    if(in_array($value,$sizes)){
                        $this->size=$value;   
                    }    
                    else {
                        $this->body='Size Is Not Valid, Please Chose OTHER size (10-24)'; #error message 
                    }
                }    
            }

            public function view(){
                echo "<html>   
                        <head>
                        <title>$this->title</title>
                        </head>
                        <body>
                        <p style = 'color:$this->color; font-size:$this->size'>$this->body</p>
                        </body
                    </html>";
            }      
        }
?>

